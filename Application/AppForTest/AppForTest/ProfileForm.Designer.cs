﻿namespace AppForTest
{
    partial class ProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.lbName = new System.Windows.Forms.Label();
            this.lbPosition = new System.Windows.Forms.Label();
            this.lbCategory = new System.Windows.Forms.Label();
            this.pbPhoto = new System.Windows.Forms.PictureBox();
            this.lbUnit = new System.Windows.Forms.Label();
            this.lbCity = new System.Windows.Forms.Label();
            this.lbBirthday = new System.Windows.Forms.Label();
            this.lbDateFrom = new System.Windows.Forms.Label();
            this.txtAbout = new System.Windows.Forms.TextBox();
            this.lbStatus = new System.Windows.Forms.Label();
            this.lbCharacter = new System.Windows.Forms.Label();
            this.lbSalary = new System.Windows.Forms.Label();
            this.lbBonus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnDelete.Location = new System.Drawing.Point(478, 368);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(163, 32);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "Уволить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnEdit.Location = new System.Drawing.Point(478, 406);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(163, 32);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Редактировать";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnBack
            // 
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnBack.Location = new System.Drawing.Point(647, 368);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(141, 70);
            this.btnBack.TabIndex = 2;
            this.btnBack.Text = "На главную";
            this.btnBack.UseVisualStyleBackColor = true;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbName.Location = new System.Drawing.Point(309, 12);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(64, 24);
            this.lbName.TabIndex = 3;
            this.lbName.Text = "ФИО: ";
            // 
            // lbPosition
            // 
            this.lbPosition.AutoSize = true;
            this.lbPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbPosition.Location = new System.Drawing.Point(309, 61);
            this.lbPosition.Name = "lbPosition";
            this.lbPosition.Size = new System.Drawing.Size(122, 24);
            this.lbPosition.TabIndex = 4;
            this.lbPosition.Text = "Должность: ";
            // 
            // lbCategory
            // 
            this.lbCategory.AutoSize = true;
            this.lbCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbCategory.Location = new System.Drawing.Point(309, 89);
            this.lbCategory.Name = "lbCategory";
            this.lbCategory.Size = new System.Drawing.Size(115, 24);
            this.lbCategory.TabIndex = 5;
            this.lbCategory.Text = "Категория: ";
            // 
            // pbPhoto
            // 
            this.pbPhoto.BackgroundImage = global::AppForTest.Properties.Resources.no_photo;
            this.pbPhoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbPhoto.Location = new System.Drawing.Point(12, 12);
            this.pbPhoto.Name = "pbPhoto";
            this.pbPhoto.Size = new System.Drawing.Size(282, 282);
            this.pbPhoto.TabIndex = 6;
            this.pbPhoto.TabStop = false;
            // 
            // lbUnit
            // 
            this.lbUnit.AutoSize = true;
            this.lbUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbUnit.Location = new System.Drawing.Point(309, 114);
            this.lbUnit.Name = "lbUnit";
            this.lbUnit.Size = new System.Drawing.Size(78, 24);
            this.lbUnit.TabIndex = 7;
            this.lbUnit.Text = "Отдел: ";
            // 
            // lbCity
            // 
            this.lbCity.AutoSize = true;
            this.lbCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbCity.Location = new System.Drawing.Point(309, 142);
            this.lbCity.Name = "lbCity";
            this.lbCity.Size = new System.Drawing.Size(76, 24);
            this.lbCity.TabIndex = 8;
            this.lbCity.Text = "Город: ";
            // 
            // lbBirthday
            // 
            this.lbBirthday.AutoSize = true;
            this.lbBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbBirthday.Location = new System.Drawing.Point(309, 37);
            this.lbBirthday.Name = "lbBirthday";
            this.lbBirthday.Size = new System.Drawing.Size(160, 24);
            this.lbBirthday.TabIndex = 9;
            this.lbBirthday.Text = "Дата рождения: ";
            // 
            // lbDateFrom
            // 
            this.lbDateFrom.AutoSize = true;
            this.lbDateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbDateFrom.Location = new System.Drawing.Point(309, 228);
            this.lbDateFrom.Name = "lbDateFrom";
            this.lbDateFrom.Size = new System.Drawing.Size(148, 24);
            this.lbDateFrom.TabIndex = 10;
            this.lbDateFrom.Text = "Трудоустроен: ";
            // 
            // txtAbout
            // 
            this.txtAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.txtAbout.Location = new System.Drawing.Point(12, 321);
            this.txtAbout.Multiline = true;
            this.txtAbout.Name = "txtAbout";
            this.txtAbout.ReadOnly = true;
            this.txtAbout.Size = new System.Drawing.Size(460, 117);
            this.txtAbout.TabIndex = 11;
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbStatus.Location = new System.Drawing.Point(309, 257);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(82, 24);
            this.lbStatus.TabIndex = 12;
            this.lbStatus.Text = "Статус: ";
            // 
            // lbCharacter
            // 
            this.lbCharacter.AutoSize = true;
            this.lbCharacter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lbCharacter.Location = new System.Drawing.Point(12, 302);
            this.lbCharacter.Name = "lbCharacter";
            this.lbCharacter.Size = new System.Drawing.Size(120, 17);
            this.lbCharacter.TabIndex = 13;
            this.lbCharacter.Text = "Характеристика:";
            // 
            // lbSalary
            // 
            this.lbSalary.AutoSize = true;
            this.lbSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbSalary.Location = new System.Drawing.Point(309, 199);
            this.lbSalary.Name = "lbSalary";
            this.lbSalary.Size = new System.Drawing.Size(76, 24);
            this.lbSalary.TabIndex = 15;
            this.lbSalary.Text = "Оклад: ";
            // 
            // lbBonus
            // 
            this.lbBonus.AutoSize = true;
            this.lbBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbBonus.Location = new System.Drawing.Point(309, 170);
            this.lbBonus.Name = "lbBonus";
            this.lbBonus.Size = new System.Drawing.Size(106, 24);
            this.lbBonus.TabIndex = 14;
            this.lbBonus.Text = "Надбавка: ";
            // 
            // ProfileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.lbSalary);
            this.Controls.Add(this.lbBonus);
            this.Controls.Add(this.lbCharacter);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.txtAbout);
            this.Controls.Add(this.lbDateFrom);
            this.Controls.Add(this.lbBirthday);
            this.Controls.Add(this.lbCity);
            this.Controls.Add(this.lbUnit);
            this.Controls.Add(this.pbPhoto);
            this.Controls.Add(this.lbCategory);
            this.Controls.Add(this.lbPosition);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnDelete);
            this.Name = "ProfileForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Production Inc.";
            this.Load += new System.EventHandler(this.ProfileForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbPosition;
        private System.Windows.Forms.Label lbCategory;
        private System.Windows.Forms.PictureBox pbPhoto;
        private System.Windows.Forms.Label lbUnit;
        private System.Windows.Forms.Label lbCity;
        private System.Windows.Forms.Label lbBirthday;
        private System.Windows.Forms.Label lbDateFrom;
        private System.Windows.Forms.TextBox txtAbout;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lbCharacter;
        private System.Windows.Forms.Label lbSalary;
        private System.Windows.Forms.Label lbBonus;
    }
}