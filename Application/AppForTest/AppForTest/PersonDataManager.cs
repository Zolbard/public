﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AppForTest
{
    [Serializable]
    class PersonDataManager
    {
        public string search;
        public List<string> unitsF;
        public List<string> positionsF;
        public List<string> categoriesF;
        public bool deletedF;
        public PersonDataManager()
        {
            unitsF = new List<string>();
            positionsF = new List<string>();
            categoriesF = new List<string>();
        }
        public void Save()
        {
            // Сохранение состояния
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream("personsState.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, this);
            }
        }
        public PersonDataManager Load()
        {
            // Загрузка состояния
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream("personsState.dat", FileMode.OpenOrCreate))
            {
                return (PersonDataManager)formatter.Deserialize(fs);
            }
        }
        public List<View_Persons> Filter(List<View_Persons> list)
        {
            // Исключение из входного листа по заявленным фильтрам
            foreach(var f in positionsF)
            {
                list.RemoveAll(x => x.Должность == f);
            }
            foreach (var f in unitsF)
            {
                list.RemoveAll(x => x.Отдел == f);
            }
            foreach (var f in categoriesF)
            {
                list.RemoveAll(x => x.Категория == f);
            }
            if(!deletedF)
            {
                list.RemoveAll(x => x.Уволен == true);
            }
            return list;
        }
        public List<View_Persons> Search(List<View_Persons> list)
        {
            // Поиск по текстовым столбцам
            return list.FindAll(x => x.ФИО.ToLower().Contains(search.ToLower()) | x.Должность.ToLower().Contains(search.ToLower()) | x.Категория.ToLower().Contains(search.ToLower()) | x.Отдел.ToLower().Contains(search.ToLower()) | x.Город.ToLower().Contains(search.ToLower()));
        }
    }
}
