﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppForTest
{
    class ImageManager:IDisposable
    {
        TestEntities db;

        public void PutImageBinaryInDb(string iFile, int personId)
        {
            // конвертация изображения в байты
            byte[] imageData = null;
            FileInfo fInfo = new FileInfo(iFile);
            long numBytes = fInfo.Length;
            FileStream fStream = new FileStream(iFile, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fStream);
            imageData = br.ReadBytes((int)numBytes);
            // получение расширения файла изображения не забыв удалить точку перед расширением
            string iImageExtension = (Path.GetExtension(iFile)).Replace(".", "").ToLower();
            // запись изображения в БД
            using (db = new TestEntities())
            {
                if(db.images.Where(x => x.personId == personId).ToList().Count>0)
                {
                    images img = db.images.Where(x => x.personId == personId).First();
                    img.image = imageData;
                    img.format = iImageExtension;
                    img.personId = personId;
                }
                else
                {
                    images img = new images();
                    img.image = imageData;
                    img.format = iImageExtension;
                    img.personId = personId;
                    db.images.Add(img);
                }
                db.SaveChanges();
            }
        }
        public Image GetImageBinaryFromDb(int personId)
        {
            try
            {
                byte[] imageData = null;
                // получаем данные из БД
                using (db = new TestEntities())
                {
                    images img = db.images.Where(x => x.personId == personId).First();
                    imageData = img.image;
                }
                // конвертируем бинарные данные в изображение
                MemoryStream ms = new MemoryStream(imageData);
                Image photo = Image.FromStream(ms);
                return photo;
            }
            catch
            {
                return null;
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
