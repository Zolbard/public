﻿namespace AppForTest
{
    partial class PersonAddEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonAddEditForm));
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtMiddleName = new System.Windows.Forms.TextBox();
            this.lbFirstName = new System.Windows.Forms.Label();
            this.lbLastName = new System.Windows.Forms.Label();
            this.lbMiddleName = new System.Windows.Forms.Label();
            this.cmbPositions = new System.Windows.Forms.ComboBox();
            this.cmbCategories = new System.Windows.Forms.ComboBox();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbBonus = new System.Windows.Forms.Label();
            this.txtResRate = new System.Windows.Forms.TextBox();
            this.lbResRate = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lbCities = new System.Windows.Forms.Label();
            this.cmbCities = new System.Windows.Forms.ComboBox();
            this.dtpBirthday = new System.Windows.Forms.DateTimePicker();
            this.txtAbout = new System.Windows.Forms.TextBox();
            this.lbAbout = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pbPhoto = new System.Windows.Forms.PictureBox();
            this.txtBonus = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBrowse
            // 
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnBrowse.Location = new System.Drawing.Point(12, 313);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(136, 46);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Обзор";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFirstName
            // 
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtFirstName.Location = new System.Drawing.Point(324, 40);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(151, 29);
            this.txtFirstName.TabIndex = 2;
            // 
            // txtLastName
            // 
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtLastName.Location = new System.Drawing.Point(481, 40);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(151, 29);
            this.txtLastName.TabIndex = 3;
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtMiddleName.Location = new System.Drawing.Point(638, 40);
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(151, 29);
            this.txtMiddleName.TabIndex = 4;
            // 
            // lbFirstName
            // 
            this.lbFirstName.AutoSize = true;
            this.lbFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbFirstName.Location = new System.Drawing.Point(320, 13);
            this.lbFirstName.Name = "lbFirstName";
            this.lbFirstName.Size = new System.Drawing.Size(58, 24);
            this.lbFirstName.TabIndex = 5;
            this.lbFirstName.Text = "Имя *";
            // 
            // lbLastName
            // 
            this.lbLastName.AutoSize = true;
            this.lbLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbLastName.Location = new System.Drawing.Point(477, 13);
            this.lbLastName.Name = "lbLastName";
            this.lbLastName.Size = new System.Drawing.Size(103, 24);
            this.lbLastName.TabIndex = 6;
            this.lbLastName.Text = "Фамилия *";
            // 
            // lbMiddleName
            // 
            this.lbMiddleName.AutoSize = true;
            this.lbMiddleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbMiddleName.Location = new System.Drawing.Point(634, 13);
            this.lbMiddleName.Name = "lbMiddleName";
            this.lbMiddleName.Size = new System.Drawing.Size(98, 24);
            this.lbMiddleName.TabIndex = 7;
            this.lbMiddleName.Text = "Отчество";
            // 
            // cmbPositions
            // 
            this.cmbPositions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.cmbPositions.FormattingEnabled = true;
            this.cmbPositions.Location = new System.Drawing.Point(324, 178);
            this.cmbPositions.Name = "cmbPositions";
            this.cmbPositions.Size = new System.Drawing.Size(151, 32);
            this.cmbPositions.TabIndex = 8;
            this.cmbPositions.SelectionChangeCommitted += new System.EventHandler(this.cmbPositions_SelectionChangeCommitted);
            // 
            // cmbCategories
            // 
            this.cmbCategories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.cmbCategories.FormattingEnabled = true;
            this.cmbCategories.Location = new System.Drawing.Point(481, 178);
            this.cmbCategories.Name = "cmbCategories";
            this.cmbCategories.Size = new System.Drawing.Size(151, 32);
            this.cmbCategories.TabIndex = 9;
            this.cmbCategories.SelectionChangeCommitted += new System.EventHandler(this.cmbCategories_SelectionChangeCommitted);
            // 
            // cmbUnits
            // 
            this.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnits.Enabled = false;
            this.cmbUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Location = new System.Drawing.Point(481, 258);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(151, 32);
            this.cmbUnits.TabIndex = 10;
            this.cmbUnits.SelectionChangeCommitted += new System.EventHandler(this.cmbUnits_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(477, 231);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 24);
            this.label1.TabIndex = 13;
            this.label1.Text = "Отдел *";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label2.Location = new System.Drawing.Point(477, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 24);
            this.label2.TabIndex = 12;
            this.label2.Text = "Категория *";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label3.Location = new System.Drawing.Point(320, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 24);
            this.label3.TabIndex = 11;
            this.label3.Text = "Должность *";
            // 
            // lbBonus
            // 
            this.lbBonus.AutoSize = true;
            this.lbBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbBonus.Location = new System.Drawing.Point(635, 151);
            this.lbBonus.Name = "lbBonus";
            this.lbBonus.Size = new System.Drawing.Size(96, 24);
            this.lbBonus.TabIndex = 15;
            this.lbBonus.Text = "Надбавка";
            // 
            // txtResRate
            // 
            this.txtResRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtResRate.Location = new System.Drawing.Point(637, 260);
            this.txtResRate.Name = "txtResRate";
            this.txtResRate.ReadOnly = true;
            this.txtResRate.Size = new System.Drawing.Size(151, 29);
            this.txtResRate.TabIndex = 16;
            // 
            // lbResRate
            // 
            this.lbResRate.AutoSize = true;
            this.lbResRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbResRate.Location = new System.Drawing.Point(633, 232);
            this.lbResRate.Name = "lbResRate";
            this.lbResRate.Size = new System.Drawing.Size(66, 24);
            this.lbResRate.TabIndex = 17;
            this.lbResRate.Text = "Оклад";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnAdd.Location = new System.Drawing.Point(12, 455);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(172, 46);
            this.btnAdd.TabIndex = 18;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnCancel.Location = new System.Drawing.Point(616, 456);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(172, 46);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lbCities
            // 
            this.lbCities.AutoSize = true;
            this.lbCities.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbCities.Location = new System.Drawing.Point(320, 231);
            this.lbCities.Name = "lbCities";
            this.lbCities.Size = new System.Drawing.Size(78, 24);
            this.lbCities.TabIndex = 21;
            this.lbCities.Text = "Город *";
            // 
            // cmbCities
            // 
            this.cmbCities.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCities.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.cmbCities.FormattingEnabled = true;
            this.cmbCities.Location = new System.Drawing.Point(324, 258);
            this.cmbCities.Name = "cmbCities";
            this.cmbCities.Size = new System.Drawing.Size(151, 32);
            this.cmbCities.TabIndex = 20;
            this.cmbCities.SelectionChangeCommitted += new System.EventHandler(this.cmbCities_SelectionChangeCommitted);
            // 
            // dtpBirthday
            // 
            this.dtpBirthday.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.dtpBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.dtpBirthday.Location = new System.Drawing.Point(501, 97);
            this.dtpBirthday.Name = "dtpBirthday";
            this.dtpBirthday.Size = new System.Drawing.Size(288, 29);
            this.dtpBirthday.TabIndex = 23;
            // 
            // txtAbout
            // 
            this.txtAbout.Location = new System.Drawing.Point(324, 326);
            this.txtAbout.Multiline = true;
            this.txtAbout.Name = "txtAbout";
            this.txtAbout.Size = new System.Drawing.Size(464, 107);
            this.txtAbout.TabIndex = 25;
            // 
            // lbAbout
            // 
            this.lbAbout.AutoSize = true;
            this.lbAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbAbout.Location = new System.Drawing.Point(320, 299);
            this.lbAbout.Name = "lbAbout";
            this.lbAbout.Size = new System.Drawing.Size(135, 24);
            this.lbAbout.TabIndex = 26;
            this.lbAbout.Text = "О сотруднике";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label4.Location = new System.Drawing.Point(320, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 24);
            this.label4.TabIndex = 27;
            this.label4.Text = "Дата рождения *";
            // 
            // pbPhoto
            // 
            this.pbPhoto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbPhoto.BackgroundImage")));
            this.pbPhoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbPhoto.InitialImage = null;
            this.pbPhoto.Location = new System.Drawing.Point(12, 12);
            this.pbPhoto.Name = "pbPhoto";
            this.pbPhoto.Size = new System.Drawing.Size(282, 282);
            this.pbPhoto.TabIndex = 0;
            this.pbPhoto.TabStop = false;
            // 
            // txtBonus
            // 
            this.txtBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtBonus.Location = new System.Drawing.Point(637, 178);
            this.txtBonus.Name = "txtBonus";
            this.txtBonus.Size = new System.Drawing.Size(151, 29);
            this.txtBonus.TabIndex = 28;
            this.txtBonus.Text = "0";
            this.txtBonus.TextChanged += new System.EventHandler(this.txtBonus_TextChanged);
            this.txtBonus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBonus_KeyPress);
            // 
            // PersonAddEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 513);
            this.ControlBox = false;
            this.Controls.Add(this.txtBonus);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbAbout);
            this.Controls.Add(this.txtAbout);
            this.Controls.Add(this.dtpBirthday);
            this.Controls.Add(this.lbCities);
            this.Controls.Add(this.cmbCities);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbResRate);
            this.Controls.Add(this.txtResRate);
            this.Controls.Add(this.lbBonus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbUnits);
            this.Controls.Add(this.cmbCategories);
            this.Controls.Add(this.cmbPositions);
            this.Controls.Add(this.lbMiddleName);
            this.Controls.Add(this.lbLastName);
            this.Controls.Add(this.lbFirstName);
            this.Controls.Add(this.txtMiddleName);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.pbPhoto);
            this.Name = "PersonAddEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Production Inc.";
            this.Load += new System.EventHandler(this.PersonAddEditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbPhoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbPhoto;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtMiddleName;
        private System.Windows.Forms.Label lbFirstName;
        private System.Windows.Forms.Label lbLastName;
        private System.Windows.Forms.Label lbMiddleName;
        private System.Windows.Forms.ComboBox cmbPositions;
        private System.Windows.Forms.ComboBox cmbCategories;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbBonus;
        private System.Windows.Forms.TextBox txtResRate;
        private System.Windows.Forms.Label lbResRate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lbCities;
        private System.Windows.Forms.ComboBox cmbCities;
        private System.Windows.Forms.DateTimePicker dtpBirthday;
        private System.Windows.Forms.TextBox txtAbout;
        private System.Windows.Forms.Label lbAbout;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBonus;
    }
}