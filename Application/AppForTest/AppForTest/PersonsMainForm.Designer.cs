﻿namespace AppForTest
{
    partial class PersonsMainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPersons = new System.Windows.Forms.DataGridView();
            this.lbTitleDgv = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lbSearch = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnProfile = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cblPositions = new System.Windows.Forms.CheckedListBox();
            this.cblCategories = new System.Windows.Forms.CheckedListBox();
            this.cblUnits = new System.Windows.Forms.CheckedListBox();
            this.lbFilters = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbDeleted = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPersons
            // 
            this.dgvPersons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPersons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersons.Location = new System.Drawing.Point(12, 206);
            this.dgvPersons.MultiSelect = false;
            this.dgvPersons.Name = "dgvPersons";
            this.dgvPersons.ReadOnly = true;
            this.dgvPersons.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPersons.Size = new System.Drawing.Size(950, 419);
            this.dgvPersons.TabIndex = 0;
            this.dgvPersons.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPersons_CellClick);
            // 
            // lbTitleDgv
            // 
            this.lbTitleDgv.AutoSize = true;
            this.lbTitleDgv.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbTitleDgv.Location = new System.Drawing.Point(8, 179);
            this.lbTitleDgv.Name = "lbTitleDgv";
            this.lbTitleDgv.Size = new System.Drawing.Size(293, 24);
            this.lbTitleDgv.TabIndex = 1;
            this.lbTitleDgv.Text = "Список сотрудников компании:";
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtSearch.Location = new System.Drawing.Point(93, 134);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(439, 29);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // lbSearch
            // 
            this.lbSearch.AutoSize = true;
            this.lbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbSearch.Location = new System.Drawing.Point(8, 137);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(76, 24);
            this.lbSearch.TabIndex = 5;
            this.lbSearch.Text = "ПОИСК";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnAdd.Location = new System.Drawing.Point(798, 165);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(164, 35);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnProfile
            // 
            this.btnProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnProfile.Location = new System.Drawing.Point(798, 114);
            this.btnProfile.Name = "btnProfile";
            this.btnProfile.Size = new System.Drawing.Size(164, 35);
            this.btnProfile.TabIndex = 7;
            this.btnProfile.Text = "Профиль";
            this.btnProfile.UseVisualStyleBackColor = true;
            this.btnProfile.Click += new System.EventHandler(this.btnProfile_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnExit.Location = new System.Drawing.Point(798, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(164, 35);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "Выход";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cblPositions
            // 
            this.cblPositions.BackColor = System.Drawing.SystemColors.Control;
            this.cblPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.cblPositions.FormattingEnabled = true;
            this.cblPositions.Location = new System.Drawing.Point(152, 36);
            this.cblPositions.Name = "cblPositions";
            this.cblPositions.Size = new System.Drawing.Size(120, 94);
            this.cblPositions.TabIndex = 9;
            this.cblPositions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.cblPositions_ItemCheck);
            // 
            // cblCategories
            // 
            this.cblCategories.BackColor = System.Drawing.SystemColors.Control;
            this.cblCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.cblCategories.FormattingEnabled = true;
            this.cblCategories.Location = new System.Drawing.Point(282, 36);
            this.cblCategories.Name = "cblCategories";
            this.cblCategories.Size = new System.Drawing.Size(120, 94);
            this.cblCategories.TabIndex = 10;
            this.cblCategories.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.cblCategories_ItemCheck);
            // 
            // cblUnits
            // 
            this.cblUnits.BackColor = System.Drawing.SystemColors.Control;
            this.cblUnits.CheckOnClick = true;
            this.cblUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.cblUnits.FormattingEnabled = true;
            this.cblUnits.Location = new System.Drawing.Point(412, 36);
            this.cblUnits.Name = "cblUnits";
            this.cblUnits.Size = new System.Drawing.Size(120, 94);
            this.cblUnits.TabIndex = 11;
            this.cblUnits.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.cblUnits_ItemCheck);
            // 
            // lbFilters
            // 
            this.lbFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbFilters.Location = new System.Drawing.Point(8, 15);
            this.lbFilters.Name = "lbFilters";
            this.lbFilters.Size = new System.Drawing.Size(138, 56);
            this.lbFilters.TabIndex = 12;
            this.lbFilters.Text = "НАСТРОЙКА ФИЛЬТРОВ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label1.Location = new System.Drawing.Point(167, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "Должности";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label2.Location = new System.Drawing.Point(301, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 18);
            this.label2.TabIndex = 14;
            this.label2.Text = "Категории";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label3.Location = new System.Drawing.Point(435, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 18);
            this.label3.TabIndex = 15;
            this.label3.Text = "Отделы";
            // 
            // cbDeleted
            // 
            this.cbDeleted.AutoSize = true;
            this.cbDeleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.cbDeleted.Location = new System.Drawing.Point(559, 36);
            this.cbDeleted.Name = "cbDeleted";
            this.cbDeleted.Size = new System.Drawing.Size(152, 21);
            this.cbDeleted.TabIndex = 16;
            this.cbDeleted.Text = "Архив сотрудников";
            this.cbDeleted.UseVisualStyleBackColor = true;
            this.cbDeleted.CheckedChanged += new System.EventHandler(this.cbDeleted_CheckedChanged);
            // 
            // PersonsMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 637);
            this.ControlBox = false;
            this.Controls.Add(this.cbDeleted);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbFilters);
            this.Controls.Add(this.cblUnits);
            this.Controls.Add(this.cblCategories);
            this.Controls.Add(this.cblPositions);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnProfile);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbSearch);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.lbTitleDgv);
            this.Controls.Add(this.dgvPersons);
            this.Name = "PersonsMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Production Inc.";
            this.Load += new System.EventHandler(this.PersonsMainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPersons;
        private System.Windows.Forms.Label lbTitleDgv;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnProfile;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.CheckedListBox cblPositions;
        private System.Windows.Forms.CheckedListBox cblCategories;
        private System.Windows.Forms.CheckedListBox cblUnits;
        private System.Windows.Forms.Label lbFilters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbDeleted;
    }
}

