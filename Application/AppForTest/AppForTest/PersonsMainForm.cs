﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppForTest
{
    [Serializable]
    public partial class PersonsMainForm : Form
    {
        // Флаг для определения производится ли загрузка данных
        bool load = false;
        TestEntities db;
        PersonDataManager pdm;
        List<View_Persons> data;
        int personId;
        public PersonsMainForm()
        {
            InitializeComponent();
            data = new List<View_Persons>();
            pdm = new PersonDataManager();
        }
        private void LoadData()
        {
            // Вывод данных в таблицу
            using(db = new TestEntities())
            {
                data = db.View_Persons.ToList();
            }
            pdm.deletedF = cbDeleted.Checked;
            data = pdm.Filter(data);
            if(txtSearch.Text.Length>0)
            {
                pdm.search = txtSearch.Text;
                data = pdm.Search(data);
            }
            dgvPersons.DataSource = data;
        }

        private void PersonsMainForm_Load(object sender, EventArgs e)
        {
            // Инициализация списков фильтров
            using (db = new TestEntities())
            {
                foreach (var position in db.positions.ToList())
                {
                    cblPositions.Items.Add(position.name);
                    cblPositions.SetItemChecked(cblPositions.Items.Count - 1, true);
                }
                foreach (var category in db.categories.ToList())
                {
                    cblCategories.Items.Add(category.name);
                    cblCategories.SetItemChecked(cblCategories.Items.Count - 1, true);
                }
                foreach (var unit in db.units.ToList())
                {
                    cblUnits.Items.Add(unit.name);
                    cblUnits.SetItemChecked(cblUnits.Items.Count - 1, true);
                }
            }
            // Запрос на загрузку данных
            if (MessageBox.Show("Хотите продолжить с последнего состояния?", "Загрузка состояния", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //Взводим флаг загрузки
                load = true;
                // Запускаем метод загрузки данных
                pdm = pdm.Load();
                //Восстанавливаем данные поиска и настроек фильтров
                txtSearch.Text = pdm.search;
                cbDeleted.Checked = pdm.deletedF;
                foreach(var pos in pdm.positionsF)
                {
                    if (cblPositions.Items.Contains(pos))
                    {
                        cblPositions.SetItemChecked(cblPositions.Items.IndexOf(pos), false);
                    }
                }
                foreach (var cat in pdm.categoriesF)
                {
                    if (cblCategories.Items.Contains(cat))
                    {
                        cblCategories.SetItemChecked(cblCategories.Items.IndexOf(cat), false);
                    }
                }
                foreach (var unit in pdm.unitsF)
                {
                    if (cblUnits.Items.Contains(unit))
                    {
                        cblUnits.SetItemChecked(cblUnits.Items.IndexOf(unit), false);
                    }
                }
                // Завершаем загрузку
                load = false;
            }
            else
            {
                pdm = new PersonDataManager();
            }
            // Загружаем данные
            LoadData();
            // Фиксируем номер сотрудника по умолчанию, если такой имеется
            if (dgvPersons.SelectedRows.Count > 0)
            {
                personId = (int)dgvPersons.SelectedRows[0].Cells[0].Value;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            // Сохраняем данные и закрываем приложение
            pdm.Save();
            Application.Exit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // вызов формы для добавления
            PersonAddEditForm frm;
            using(frm = new PersonAddEditForm())
            {
                this.Hide();
                if(frm.ShowDialog() == DialogResult.OK)
                {
                    //Обновление данных в случае успешного диалога
                    LoadData();
                }
                this.Show();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            // Обновление поиска
            pdm.search = txtSearch.Text;
            LoadData();
        }

        private void cblPositions_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // Обновление списков фильтров в случае, если не происходит загрузка данных
            if (!load)
            {
                if (e.NewValue == CheckState.Unchecked)
                {
                    pdm.positionsF.Add(cblPositions.Items[e.Index].ToString());
                    MessageBox.Show("Фильтр должностей добавлен " + cblPositions.Items[e.Index].ToString());
                    LoadData();
                }
                else if (e.NewValue == CheckState.Checked)
                {
                    if (pdm.positionsF.Contains(cblPositions.Items[e.Index].ToString()))
                    {
                        pdm.positionsF.Remove(cblPositions.Items[e.Index].ToString());
                        LoadData();
                    }
                }
            }
        }

        private void cblCategories_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // Обновление списков фильтров в случае, если не происходит загрузка данных
            if (!load)
            {
                if (e.NewValue == CheckState.Unchecked)
                {
                    pdm.categoriesF.Add(cblCategories.Items[e.Index].ToString());
                    MessageBox.Show("Фильтр категорий добавлен " + cblCategories.Items[e.Index].ToString());
                    LoadData();
                }
                else if (e.NewValue == CheckState.Checked)
                {
                    if (pdm.categoriesF.Contains(cblCategories.Items[e.Index].ToString()))
                    {
                        pdm.categoriesF.Remove(cblCategories.Items[e.Index].ToString());
                        LoadData();
                    }
                }
            }
        }

        private void cblUnits_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // Обновление списков фильтров в случае, если не происходит загрузка данных
            if (!load)
            {
                if (e.NewValue == CheckState.Unchecked)
                {
                    pdm.unitsF.Add(cblUnits.Items[e.Index].ToString());
                    MessageBox.Show("Фильтр отдела добавлен " + cblUnits.Items[e.Index].ToString());
                    LoadData();
                }
                else if (e.NewValue == CheckState.Checked)
                {
                    if (pdm.unitsF.Contains(cblUnits.Items[e.Index].ToString()))
                    {
                        pdm.unitsF.Remove(cblUnits.Items[e.Index].ToString());
                        LoadData();
                    }
                }
            }
        }

        private void cbDeleted_CheckedChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgvPersons_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            personId = (int)dgvPersons.SelectedRows[0].Cells[0].Value;
        }

        private void btnProfile_Click(object sender, EventArgs e)
        {
            // Вызов формы профиля сотрудника
            ProfileForm frm;
            using(frm = new ProfileForm(personId))
            {
                this.Hide();
                frm.ShowDialog();
                LoadData();
                this.Show();
            }
        }
    }
}
