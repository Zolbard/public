﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppForTest
{
    public partial class ProfileForm : Form
    {
        int personId;
        persons pers;
        ImageManager im;
        TestEntities db;
        public ProfileForm(int personId)
        {
            InitializeComponent();
            this.personId = personId;
        }
        private void LoadData()
        {
            // Загрузка данных сотрудника
            using (db = new TestEntities())
            {
                pers = db.persons.Find(personId);
                lbName.Text = "ФИО: " + pers.firstName + " " + pers.lastName + " " + pers.middleName;

                positions pos = db.positions.Where(x => x.id == pers.positionId).First();
                lbPosition.Text = "Должность: " + pos.name;

                categories cat = db.categories.Where(x => x.id == pers.categoryId).First();
                lbCategory.Text = "Категория: " + cat.name;

                cities city = db.cities.Where(x => x.id == pers.cityId).First();
                lbCity.Text = "Город: " + city.name;

                units unit = db.units.Where(x => x.id == pers.unitId).First();
                lbUnit.Text = "Отдел: " + unit.name;

                lbBirthday.Text = "Дата рождения: " + pers.birthday.ToString("D");
                DateTime date = (DateTime)pers.dateFrom;
                lbDateFrom.Text = "Трудоустроен: " + date.ToString("D");
                txtAbout.Text = pers.description;
                lbBonus.Text = "Надбавка: " + pers.bonus.ToString() + "руб.";
                lbSalary.Text = "Оклад: " + pers.salary.ToString() + "руб.";
                if (pers.deleted)
                {
                    date = (DateTime)pers.dateTo;
                    lbStatus.Text = "Статус: уволен от " + date.ToString("D");
                    btnDelete.Enabled = false;
                    btnEdit.Enabled = false;
                }
                else
                {
                    lbStatus.Text = "Статус: актуальный сотрудник";
                }
                // Выгрузка фотографии
                using (im = new ImageManager())
                {
                    if(im.GetImageBinaryFromDb(personId)!=null)
                    {
                        pbPhoto.BackgroundImage = im.GetImageBinaryFromDb(personId);
                    }
                }
            }
        }
        private void ProfileForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            // Вызов формы редактирования
            PersonAddEditForm frm;
            using(frm = new PersonAddEditForm(personId))
            {
                this.Hide();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    LoadData();
                }
                this.Show();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Увольнение сотрудника
            if(MessageBox.Show("Вы действительно хотите уволить данного сотрудника?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                using (db = new TestEntities())
                {
                    pers = db.persons.Find(personId);
                    pers.deleted = true;
                    pers.dateTo = DateTime.Now;
                    db.SaveChanges();
                    MessageBox.Show("Сотрудник был успешно уволен!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadData();
                }
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
