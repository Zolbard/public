﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppForTest
{
    public partial class PersonAddEditForm : Form
    {
        TestEntities db;
        ImageManager im;
        string file = null;
        // Флаг для определения действия на форме
        bool update = false;
        // Номер сотрудника
        int personId;
        // Переменные для рассчета оклада
        decimal salary = 0;
        decimal CatMult = 1;
        decimal UnitMult = 1;
        // Конструктор для добовления сотрудника
        public PersonAddEditForm()
        {
            InitializeComponent();
            cmbUnits.Enabled = false;
        }
        // Конструктор для обновления данных о сотруднике
        public PersonAddEditForm(int personId)
        {
            InitializeComponent();
            update = true;
            this.personId = personId;
            btnAdd.Text = "Сохранить";
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // Отрываем проводник для загрузки фото
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult res = ofd.ShowDialog();
            if (res == DialogResult.OK)
            {
                file = ofd.FileName;
                string ext = Path.GetExtension(file).Replace(".", "").ToLower();
                if (ext == "jpg" | ext == "png")
                {
                    pbPhoto.BackgroundImage = new Bitmap(file);
                }
                else
                {
                    MessageBox.Show("Пожалуйста, выберите фотографию с расширением jpg или png!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using(db = new TestEntities())
            {
                try
                {
                    if (update)
                    {
                        // Обновляем информацию о сотруднике
                        persons pers = db.persons.Find(personId);
                        pers.firstName = txtFirstName.Text;
                        pers.lastName = txtLastName.Text;
                        pers.middleName = txtMiddleName.Text;
                        pers.positionId = (int)cmbPositions.SelectedValue;
                        pers.categoryId = (int)cmbCategories.SelectedValue;
                        pers.unitId = (int)cmbUnits.SelectedValue;
                        pers.bonus = Convert.ToDecimal(txtBonus.Text);
                        pers.cityId = (int)cmbCities.SelectedValue;
                        pers.birthday = dtpBirthday.Value;
                        pers.description = txtAbout.Text;
                        pers.salary = Convert.ToDecimal(txtResRate.Text);
                        db.SaveChanges();
                        MessageBox.Show("Данные о сотруднике успешно обновлены!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);   
                    }
                    else
                    {
                        // Создаем нового сотрудника и заполняем его данные
                        persons pers = new persons();
                        pers.firstName = txtFirstName.Text;
                        pers.lastName = txtLastName.Text;
                        pers.middleName = txtMiddleName.Text;
                        pers.positionId = (int)cmbPositions.SelectedValue;
                        pers.categoryId = (int)cmbCategories.SelectedValue;
                        pers.unitId = (int)cmbUnits.SelectedValue;
                        pers.cityId = (int)cmbCities.SelectedValue;
                        pers.bonus = Convert.ToDecimal(txtBonus.Text);
                        pers.birthday = dtpBirthday.Value;
                        pers.description = txtAbout.Text;
                        pers.dateFrom = DateTime.Now;
                        pers.salary = Convert.ToDecimal(txtResRate.Text);
                        db.persons.Add(pers);
                        db.SaveChanges();
                        MessageBox.Show("Данные о новом сотруднике успешно добавлены!", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    this.DialogResult = DialogResult.OK;
                }
                catch
                {
                    MessageBox.Show("Не удалось сохранить данные! Пожалуйста проверьте ввод данных. Все обязательные поля для заполнения помечены *", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if(file!=null)
            {
                // Добавляем фотографию или обновляем старую
                using (im = new ImageManager())
                {
                    if(personId>0)
                    {
                        im.PutImageBinaryInDb(file, personId);
                    }
                    else
                    {
                        using(db = new TestEntities())
                        {
                            persons pers = db.persons.OrderByDescending(x=>x.id).First();
                            im.PutImageBinaryInDb(file, pers.id);
                        }
                    }
                        
                }
            }
        }

        private void PersonAddEditForm_Load(object sender, EventArgs e)
        {
            dtpBirthday.MaxDate = DateTime.Today.AddYears(-18);
            using(db = new TestEntities())
            {
                // Привязываем данные таблиц к выпадающим спискам
                cmbPositions.DataSource = db.positions.ToList();
                cmbPositions.DisplayMember = "name";
                cmbPositions.ValueMember = "id";
                cmbPositions.SelectedIndex = -1;

                cmbCategories.DataSource = db.categories.ToList();
                cmbCategories.DisplayMember = "name";
                cmbCategories.ValueMember = "id";
                cmbCategories.SelectedIndex = -1;

                cmbCities.DataSource = db.cities.ToList();
                cmbCities.DisplayMember = "name";
                cmbCities.ValueMember = "id";
                cmbCities.SelectedIndex = -1;

                if(update)
                {
                    // Восстанавливаем данные о сотруднике
                    persons pers = db.persons.Find(personId);
                    txtFirstName.Text = pers.firstName;
                    txtLastName.Text = pers.lastName;
                    txtMiddleName.Text = pers.middleName;
                    txtAbout.Text = pers.description;
                    txtBonus.Text = pers.bonus.ToString();
                    dtpBirthday.Value = pers.birthday;
                    cmbPositions.SelectedValue = pers.positionId;
                    cmbCategories.SelectedValue = pers.categoryId;
                    cmbCities.SelectedValue = pers.cityId;
                    // Загружаем список отделов города
                    UpdateUnits();
                    cmbUnits.SelectedValue = pers.unitId;
                    // Рассчитываем оклад
                    using(db = new TestEntities())
                    {
                        positions pos = db.positions.Find(cmbPositions.SelectedValue);
                        salary = pos.rate;
                        categories cat = db.categories.Find(cmbCategories.SelectedValue);
                        CatMult = cat.multiplier;
                        units unit = db.units.Find(cmbUnits.SelectedValue);
                        UnitMult = unit.multiplier;
                        txtResRate.Text = CalcSalary().ToString("#.00");
                    }
                    // Загружаем фото
                    using (im = new ImageManager())
                    {
                        if (im.GetImageBinaryFromDb(personId) != null)
                        {
                            pbPhoto.BackgroundImage = im.GetImageBinaryFromDb(personId);
                        }
                    }
                }
            }
        }
        // Метод для обновления списка доступных отделов в зависимости от выбранного города
        private void UpdateUnits()
        {
            using (db = new TestEntities())
            {
                List<UnitsInCity> uic = db.UnitsInCity.Where(x => x.cityId == (int)cmbCities.SelectedValue).ToList();
                List<units> unitList = new List<units>();
                foreach (var unit in uic)
                {
                    unitList.Add(db.units.Find(unit.unitId));
                }
                cmbUnits.DataSource = unitList;
                cmbUnits.DisplayMember = "name";
                cmbUnits.ValueMember = "id";
                cmbUnits.SelectedIndex = -1;
                cmbUnits.Enabled = true;
            }
        }
        private void cmbCities_SelectionChangeCommitted(object sender, EventArgs e)
        {
            UpdateUnits();
        }

        private void cmbPositions_SelectionChangeCommitted(object sender, EventArgs e)
        {
            using(db = new TestEntities())
            {
                positions pos = db.positions.Find(cmbPositions.SelectedValue);
                salary = pos.rate;
            }
            txtResRate.Text = CalcSalary().ToString("#.00");
        }
        private decimal CalcSalary()
        {
            return salary * CatMult * UnitMult + Convert.ToDecimal(txtBonus.Text);
        }

        private void cmbCategories_SelectionChangeCommitted(object sender, EventArgs e)
        {
            using (db = new TestEntities())
            {
                categories cat = db.categories.Find(cmbCategories.SelectedValue);
                CatMult = cat.multiplier;
            }
            txtResRate.Text = CalcSalary().ToString("#.00");
        }

        private void cmbUnits_SelectionChangeCommitted(object sender, EventArgs e)
        {
            using (db = new TestEntities())
            {
                units unit = db.units.Find(cmbUnits.SelectedValue);
                UnitMult = unit.multiplier;
            }
            txtResRate.Text = CalcSalary().ToString("#.00");
        }

        private void txtBonus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(Char.IsDigit(e.KeyChar) | Char.IsControl(e.KeyChar))
            {
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtBonus_TextChanged(object sender, EventArgs e)
        {
            if(txtBonus.Text=="")
            {
                txtBonus.Text = "0";
            }
            txtResRate.Text = CalcSalary().ToString("#.00");
        }
    }
}